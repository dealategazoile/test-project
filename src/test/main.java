package test;

import java.text.SimpleDateFormat;
import java.time.temporal.JulianFields;
import java.time.temporal.TemporalField;
import java.util.Date;


public class main {

	public static void main(String[] args) {
		/*
		 *   
   d = jd - 2451545.0;  // jd is the given Julian date 

   g = 357.529 + 0.98560028* d;
   q = 280.459 + 0.98564736* d;
   L = q + 1.915* sin(g) + 0.020* sin(2*g);

   R = 1.00014 - 0.01671* cos(g) - 0.00014* cos(2*g);
   e = 23.439 - 0.00000036* d;
   RA = arctan2(cos(e)* sin(L), cos(L))/ 15;

   D = arcsin(sin(e)* sin(L));  // declination of the Sun
   EqT = q/15 - RA;  // equation of time
		 */

		Date date = new Date();
		String julianDateString = new SimpleDateFormat("yyD").format(date);
		System.out.println(julianDateString);
		
		Double jd = Double.parseDouble(julianDateString);
		
		//System.out.print(jd);
		Double d = jd - 2451545.0;  // jd is the given Julian date 
		Double g = 357.529 + 0.98560028* d;
		Double q = 280.459 + 0.98564736* d;
		Double L = q + 1.915* Math.sin(g) + 0.020* Math.sin(2*g);
		Double R = 1.00014 - 0.01671* Math.cos(g) - 0.00014* Math.cos(2*g);
		Double e = 23.439 - 0.00000036* d;
		Double RA = Math.atan2(Math.cos(e)* Math.sin(L), Math.cos(L))/ 15;
		Double D = Math.asin(Math.sin(e)* Math.sin(L));  // declination of the Sun
		Double EqT = q/15 - RA;  // equation of time
		Double Latitude = 36.6389;
		Double Longitude= 2.7685;
		Double TimeZone = 1.0;
		//----------------------------------------------
		Double Dhuhr = 12 + TimeZone - Longitude/15 - EqT;
		System.out.println(Dhuhr);
		String pattern = " HH:mm:ss.SSSZ dd/mm/yyyy";
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
		String example = simpleDateFormat.format(Dhuhr);
		System.out.println("dohr " + example);
		
		
		
		
		//----------------------------------------------
		Double Sunrise = Dhuhr - T(0.833,L,D);
		Double Sunset = Dhuhr + T(0.833,L,D); 
		String examplesunrise = simpleDateFormat.format(Sunrise);
		System.out.println(examplesunrise);
		System.out.println(Sunrise);
		String exampleSunSet = simpleDateFormat.format(Sunset);
		System.out.println(exampleSunSet);
		System.out.println(Sunset);

	}
	
	public static Double T(Double alpha , Double L , Double D) {
		Double t = (1/15)*Math.acos((-Math.sin(alpha)-Math.sin(L)*Math.sin(D))/(Math.cos(L)*Math.cos(D)));
		
		return t;
	}

}
